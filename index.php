<!DOCTYPE html>
<html>
<title>UGrokIt - Kopipészterek</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet">
<link href="/style.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<body>
<?php
$con = mysqli_connect( "79.172.252.111", "honlapva_szoft", "+UAL.}2&u+.S", "honlapva_szoftfejl" );

if ( mysqli_connect_errno( $con ) ) {
	echo "Failed to connect to MySQL: " . mysqli_connect_error();
}
mysqli_set_charset( $con, "utf8" );

$result  = $con->query( "SELECT * FROM rfid_tags AS t LEFT JOIN rfid_tags AS r ON r.rfid_id = t.rfid_id" );
$results = [];

if ( $result->num_rows > 0 ) {
	while ( $row = $result->fetch_assoc() ) {
		$results[] = $row;
	}
}
mysqli_close( $con );
?>
<div class="container mt-5 mb-5">
    <div class="row">
        <div class="col-md-6">
            <h4>A begyűjtött adatok kronológikus megjelenítése</h4>
            <ul class="timeline">
				<?php foreach ( $results as $item ): ?>
                    <li>
                        <h6><?= $item['name'] ?></h6>
                        <a href="#" class="float-right"><?= $item['created_at'] ?></a>
                        <p>ID: <?= $item['rfid_id'] ?></p>
                    </li>
				<?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
</body>
</html>